# BibliotecariosAlSenado

Aquí encontrarás la Metodología de trabajo distribuida de Bibliotecarios Al Senado.

Si vas a hacer una derivación de nuestra metodología, aquí el texto de cómo lo hicimos nosotros. Nos gustaría compartir esta metodología para que al final consolidemos una comunidad bibliotecaria unida.

Puedes escribirnos a [nuestro correo](mailto:bibliotecariosalsenado@lists.riseup.net) para que estés al tanto de las últimas conversaciones sobre el desarrollo de esta metodología.
<!-- 
La fuente de este documento se encuentra en https://docutopia.tupale.co/s/bibliotecariosalsenado-trabajo-distribuido

Este documento se genera a partir de una de las tareas de las reuniones quincenales de Bibliotecarios Al Senado. Es el manual de trabajo distribuido y anexo del documento marco: "Bibliotecarios Al Senado: colectivo de cultura y acción política bibliotecaria".
--> 

# Metodología de trabajo distribuido de Bibliotecarios Al Senado

![Logo de Bibliotecarios Al Senado](http://blog.nomono.co/wp-content/uploads/2018/05/Logo_BibliotecariosAlSenado_Fondo-Blanco.png)

En Bibliotecarios Al Senado adoptamos una metodología de trabajo distribuido, lo que implica autonomía por parte de los integrantes. Nos basamos en una versión propia adaptada de [Open Leadership de Mozilla](https://mozilla.github.io/open-leadership-training-series/)

## El trabajo distribuido
* Trabajamos desde la ***comunidad bibliotecaria*** y en acciones de ***cultura política***. Somos una ***comunidad de práctica***.
* Mantenemos una ***organización plana, evitando las jerarquías***. Todas las personas y participantes están en igualdad de condiciones para proponer y desarrollar acciones. 
<!-- podríamos pensar, sobre el punto anterior algún tipo de 
delimitación de la libertades. Algo como "es libre de hacer en 
nombre del colectivo, siempre y cuando..." no se atente contra el
bien común o del colectivo y las instituciones que representa, por ejemplo.
-->
* Desarrollamos ***proyectos abiertos***, para que sean diversos, inclusivos y replicables.
* ***Documentamos en Internet***. Usamos plataformas abiertas publicamos en dominio público.
* ***Enseñamos y aprendemos***. Evaluamos nuestra experiencia y mejoramos nuestras prácticas para el próximo proyecto.
* Seguimos ***metodologías propias***: más adelante las explicamos y desarrollamos.
    
## Condiciones de participación
Para hacer parte del grupo base y participar en las reuniones quincenales se requiere:
* Que dos integrantes activos propongan la participación de un nuevo integrante en la reunión quincenal.
* Que quien quiera participar redacte y proponga un documento con una actividad concreta a realizar.

## Metodología 
Tenemos metodologías para las acciones que realizamos.
* Metodología general
* Metodología Vélez para acciones políticas
* Metodología Jaramillo para acciones políticas

### Metodología general
* Proponemos un tema en nuestra reunión periódica. 
    * Diseñamos un plan de manera conjunta: Qué vamos a hacer, cómo, quiénes, en cuanto tiempo y cuál es el resultado. *Debe haber un responsable del plan*.
    * Si el plan incluye financiación, una parte de la financiación va al colectivo, para que él decida en qué invertirlo a futuro.
    * Invitamos a otros a participar en el proyecto, documentando las ideas o principios de la acción.
    * Realizamos la acción política en la vida real (no sólo en Internet).
        * Comunicaciones
        * Eventos
        * Reuniones
        * Declaraciones
    * Evaluamos, documentamos y publicamos los resultados (divulgación).

### Metodología Vélez para acciones políticas

Basada en las sugerencias de Alejandra Vélez en la actualización de la ley de derecho de autor en Colombia del 2018.

> Con querendura mejor
> Alejandra Vélez

Ésta metodología hace referencia a la construcción de debates mucho más amables a pesar de la discrepancia en las opiniones.

### Metodología Jaramillo para acciones políticas

Basada en [la presentación de Johana Jaramillo en el Jueves de ASEIBI](http://blog.nomono.co/2018/05/27/documentos-presentados-en-el-jueves-de-aseibi/)

* Arme su parche
* Identifique el problema
  * Encuentre los documentos fuente: proyectos de ley, artículos que se desean incluir o modificar y las versiones anteriores, si existen.
* Identifique los casos
    * Responda a la pregunta ¿en qué situaciones afecta el cambio de la ley? 
* Identifique la legislación
  * Use herramientas de comentarios (como Hypothes.is) sobre los documentos fuente
* Generar el articulado (los artículos de la ley)
* Arme un parche más grande
    * Explícale a tu parche
        * Definir intención comunicativa o mensaje.
        * Crear un lema o bandera
        * Post en Nomono
        * Video explicativo
        * Memes  y piezas gráficas
        * Conversatorio
    * Ábrele camino a tu parche
        * Cartas a entidades de gobierno
        * Cartas al Congreso de la República
        * Valla a las entidades y explique la situación
    * Somos un solo parche
        * Solicitar apoyo a redes, asociaciones, empresas de base bibliotecológica y escuelas.
        * Identificar aliados posibles en otros sectores aliados
            * Gestione las alianzas y arme un gran parche
        * Identificar posibles opositores
            * Defina acciones de control para opositores
    * Negocia con tu parche
        * Acciones de presencia gremial en la negociación
            * Vincular a las UTL de congresistas con comunicado sencillo
            * Ir al congreso y explicar la situación
    * Haz fuerza con tu parche
        * Asistir a las plenarias
        * Preparar carteles y estrategia de redes para plenaria.
    * Promueve que el movimiento y el trabajo gremial
        * Comparte la filosofía
        * Promociona las metodologías
        * Promueve el trabajo colaborativo
        * Promueve la querendura
